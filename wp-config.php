<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */


// ===================================================
// Load database info and local development parameters
// ===================================================

if ( file_exists( dirname( __FILE__ ) . '/local-config.php' ) ) {
    define( 'WP_LOCAL_DEV', true );
//    define( 'WP_DEBUG', true );
    include(dirname(__FILE__) . '/local-config.php');
} else {
    define( 'WP_LOCAL_DEV', false );

    if (preg_match('/dreamonmattresses.co.uk$/', $_SERVER['HTTP_HOST'])) {
        define( 'ENV', 'production' );
    } else  if (preg_match('/sbdigi.com$/', $_SERVER['HTTP_HOST'])) {
        define( 'ENV', 'stage' );
    } else  if (preg_match('/dreamon.dev/', $_SERVER['HTTP_HOST'])) {
        define( 'ENV', 'dev-thomas' );
    } else  {
        define( 'ENV', 'live' );
    }

    switch (ENV) {
        case 'production':
            define('DB_NAME', 'dreamon');
            define('DB_USER', 'dreamon');
            define('DB_PASSWORD', 'fr33styl3');
            define('DB_HOST', 'cust-mysql-123-17');
            break;

        case 'stage':
            define('DB_NAME', 'dreamon');
            define('DB_USER', 'dreamon_usr');
            define('DB_PASSWORD', 'fr33styl3');
            define('DB_HOST', 'sandboxdigital.cfymqgooqroi.ap-southeast-2.rds.amazonaws.com');
            break;

        case 'dev-thomas':
            define('DB_NAME', 'dreamon');
            define('DB_USER', 'root');
            define('DB_PASSWORD', 'root');
            define('DB_HOST', 'localhost');
            break;

        default:
            define('DB_NAME', 'dreamon');
            define('DB_USER', 'root');
            define('DB_PASSWORD', '');
            define('DB_HOST', 'localhost');
            break;
    }
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'mb^56/hNIGcS` )0uXa&8JSAY)IytE EO.;5b|ui srH}BfqC( q,k)4Xe>-Uq|>');
define('SECURE_AUTH_KEY',  '&[$|3}Fo~,W0@ir)F$3x^ [;$BKwMf o{XF:e^+^ilS/CR|l*~xYPT+@?h.|@SdG');
define('LOGGED_IN_KEY',    'NUbwlhc3%-Z|nO]La=99!ll%ydB(1)k;YBn.P<<*Q43t+eDn85UH3KA5hCmO{iN~');
define('NONCE_KEY',        's9QtT|c9p3u%%Wrbi+rcy-J2|Lf4<^zy:K@mO#ibG40_{jK2n*,VN|~`VC`MKpFt');
define('AUTH_SALT',        'BoWQDkjEi4t|WMbF}ILk)Nap^s{(HaQwbz/_~0{s[|?}SNA)-k=-})+ J1 Z<E>H');
define('SECURE_AUTH_SALT', '^v|7I}ipRoY~u3&r=3l-9wp7-``/UY#w :6ft>88=ltA)?(~iha7C#E:6R+q|{DN');
define('LOGGED_IN_SALT',   '^zVmoA6COH_(WS3CJ,7Y!hgo|SFX3!J<EnL`DQY:AONxUG~9c>9B:TE]!-%Q2+8X');
define('NONCE_SALT',       '.&6qqUvX|[<V]~{g>{3P*=+KSk@4;X` vT_-(6i#h1tgv-Rm<I|fP>>wi=9+{MwO');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
