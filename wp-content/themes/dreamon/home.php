<?php get_header(); ?>
<div id="main">
	<div id="social">
		<span>Share</span>
		<a href="#twModal" class="tw"></a>
		<a href="#" class="fb" id="share_button"></a>

	</div>
	<section id="slider">
		<ul id="images">	
			<?php query_posts("post_type=slider&orderby=date&order=ASC");
			if(have_posts()) : while(have_posts()): the_post(); ?>
			<li>
				<h2><?php the_field('headline') ?></h2>
				<img class="img-responsive" src="<?php get_featured_image("post_id=".$post->ID."&display=0") ?>" />
			</li>
			<?php endwhile; endif; ?>	
		</ul>
	</section>

	<section id="content">
		<div id="notch-content"></div>
		<h2>Hello There<br />
		<span>we are Dream on</span></h2>
		<p>We create great mattresses, mattress toppers and pillows. With over
			<a href="<?php echo get_permalink(ABOUT_US_ID) ?>">12 years experience</a>, we have built an extensive knowledge of what the body needs for a
			good night’s sleep. It is with this knowledge that we have built a
			<a href="<?php echo get_permalink(OUR_RANGE) ?>">range of products</a> to suit everyone’s needs.</p>
		    <a href="<?php echo get_permalink(OUR_RANGE) ?>" class="button">Our range</a>
	</section>
</div>
<div id="fb-root"></div>
<script>
window.fbAsyncInit = function() {
FB.init({appId: '504070299687053', status: true, cookie: true,
xfbml: true});
};
(function() {
var e = document.createElement('script'); e.async = true;
e.src = document.location.protocol +
'//connect.facebook.net/en_US/all.js';
document.getElementById('fb-root').appendChild(e);
}());
</script>
<?php get_footer(); ?>