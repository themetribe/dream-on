<?php get_header() ?>

<div id="main">
	<?php 
	if(have_posts()) : while(have_posts()): the_post(); 
		$sub_title = get_field("sub_title");
	?>
		<header class="vcenter">
			<div>
				<h2><?php the_title() ?></h2>
				<small><?php echo $sub_title ?></small>
			</div>
		</header>



		<section id="content">
			<div id="notch-content"></div>
			<div class="container">
				<?php the_content() ?>
			</div>
		</section>
	<?php endwhile; endif; ?>
</div>

<?php get_footer() ?>