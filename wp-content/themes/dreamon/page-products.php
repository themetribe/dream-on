<?php
/** Template Name: Products  */
get_header(); ?>
<div id="main">
	<div id="products-details">
		<a href="#" class="close-detail">X</a>
		<?php 
		query_posts("post_type=products&orderby=date&order=ASC");
		if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="entry clearfix p<?php the_ID() ?>">
			<div class="image">
                <?php $img = get_field('product_hover_image'); ?>
                <?php
                if ($img) {
                ?>
				<strong class="ipad-hide">- ROLLOVER to see the layers -</strong>
				<strong class="ipad">- Click to see the layers -</strong>
				<img class="hover" src="<?php echo $img ?>" />
                <?php
                }
                ?>
				<?php get_featured_image("post_id=".$post->ID) ?>
			</div>
			<div class="details">
                <?php
                $key_details = get_field('key_details');
                $key_features = get_field('key_features');
                ?>
                <h3 class="product-name"><?php the_field('description_title') ?></h3>
                <div class="basic-info scroll">
                    <ol>
                        <?php foreach($key_details as $val) : ?>
                            <li><?php echo $val['text'] ?></li>
                        <?php endforeach; ?>
                    </ol>
                    <h4>Key Features</h4>
                    <ul>
                        <?php foreach($key_features as $val) : ?>
                            <li><?php echo $val['text'] ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <p class="more-info-button"><a href="#" class="button more-info"><span>More Info</span></a></p>
                <div class="more-info">
                    <p align="center" style="margin-bottom:30px;"><a href="#" class="button less-info"><span>Less Info</span></a></p>
                    <div class="content scroll"><?php the_content() ?></div>
                </div>
			</div>	
		</div>
		<?php endwhile; endif; wp_reset_query(); ?>
	</div>

	<div id="mobile-details">
		<a href="#" class="close-detail">X</a>
		<?php 
		query_posts("post_type=products&orderby=date&order=ASC");
		if(have_posts()) : while(have_posts()): the_post(); 
			$key_details = get_field('key_details');
			$key_features = get_field('key_features');
			?>
		<div class="entry clearfix p<?php the_ID() ?>">
			<h3 class="product-name"><?php the_field('description_title') ?></h3>
			
			<?php $img = get_field('product_mobile_image'); ?>	
			<img class="hover" src="<?php echo $img ?>" />
			<div class="details">
				<ol>
					<?php foreach($key_details as $val) : ?>
						<li><?php echo $val['text'] ?></li>
					<?php endforeach; ?>
				</ol>
				<h4>Key Features</h4>
				<ul>
					<?php foreach($key_features as $val) : ?>
						<li><?php echo $val['text'] ?></li>
					<?php endforeach; ?>
				</ul>
				<hr />
				<div class="content"><?php the_content() ?></div>
			</div>	
		</div>
		<?php endwhile; endif; wp_reset_query(); ?>
	</div>










	<div id="products-thumb" class="clearfix">
		<?php 
		query_posts("post_type=products&orderby=date&order=ASC");
		if(have_posts()) : while(have_posts()): the_post(); ?>
		<div class="entry">
			<a href="#" data-post-id="<?php the_ID() ?>"><h3><?php the_title(); ?></h3></a>
			<div class="thumb"><?php get_featured_image("post_id=".$post->ID."&size=large&h=295&w=350") ?></div>
			
		</div>
		<?php endwhile; endif; wp_reset_query(); ?>
	</div>

</div>
<?php get_footer() ?>