<?php
/** Template Name: Contact Us Page  */
get_header(); ?>
<script type="text/javascript">google.maps.event.addDomListener(window, 'load', initialize);</script>



<style>
	
</style>
<div id="main">
	<div id="gmap">
		<a id="map-locator"></a>
   		<div id="map-canvas"></div>
	</div>
	<div id="content">
		<div id="notch-content"></div>
		<div class="container">
			<div class="one_half">
				<h3>Come in and<br />
				<span>speak with us</span></h3>
				<p>
					Dream On<br />
					Unit 1, Dial Post Park<br />
					Horsham Road<br />
					Rusper<br />
					West Sussex<br />
					RH12 4QX
				</p>
			</div>
			<div class="one_half last">
				<h3>Give us a Call</h3>
				<a href="tel://01403-000-000" class="phone-number mobile">01403 000 000</a>
				<a class="phone-number">01403 000 000</a>
				<hr />
				<h3>Drop Us A Line</h3>
				<p><a href="mailto:name@dreamon.com" class="button">Email Us</a></p>
				<p><em><a href="mailto:name@dreamon.com" class="link">name@dreamon.com</a></em></p>

			</div>
		</div>
	</div>
</div>
<?php get_footer();
