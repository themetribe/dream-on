<?php
header('X-Frame-Options: GOFORIT');
/* ----------------------------------------------- [ #1 : Don't remove this ] ------------------------*/

require('base/base.php');
include('lib/responsive.php');

register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'primary', __( 'Primary Menu', 'atom' ) );
register_nav_menu( 'footer', __( 'Footer Menu', 'atom' ) );

add_theme_support( 'post-thumbnails');
add_theme_support( 'automatic-feed-links' );

/* ----------------------------------------------- [/ #1 ] ------------------------*/

define("ABOUT_US_ID",6);
define("OUR_RANGE",4);

?>