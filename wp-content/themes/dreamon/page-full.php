<?php
/** Template Name: Full Width  */
get_header(); ?>

<div id="main">
	<div class='container'>
		<div id="content" role="main">
			<?php
			if(have_posts()) : while(have_posts()): the_post(); ?>
			<div class="clearfix entry">        	
	            <div class="excerpt">
	                <?php the_content() ?>
	            </div>
	        </div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>