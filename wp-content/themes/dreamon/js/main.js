var map;
var ipad_size = 767;
var mobile_size = 480;
var lat = 51.116377;
var lon = -0.289412;

(function(global, $){	
	$(window).resize(function() {		
		menu($,true);
		home_page($,true);
		our_range_page($,true);
		contact_page($,true);
	});
	$(document).ready(function(){  
		
		menu($);
		home_page($);
		our_range_page($);
		about_page($);
		contact_page($);
	});
})(window, jQuery);

//================================================== HOME PAGE
function home_page($,resize){
	if(resize){
		if($('body').hasClass('home')){
			$('#images').cycle('destroy');
			if($("#main").hasClass('full')){
				$("#main, #images").css({
					marginLeft:0,
					width:"100%"
				});
				_height = $("#images img").height();
				_width = $('#images').parent().width();
			  	$("#images").height(_height);
			    $('#images').cycle({
			    	fit : 1,
			        containerResize: 1,
			        width: _width,
			        after: function(curr, next, obj) {
			            window.activeSlide = obj.currSlide;
			        },
			        startingSlide: window.activeSlide
			    });
			}
			else{

				setTimeout(function(){				
					$("#main, #images").width( $(window).width()-330 );
					$("#main").css("margin-left","330px");
					_height = $("#images img").height();
					_width = $('#images').parent().width();
				  	$("#images").height(_height);
				    $('#images').cycle({
				    	fit : 1,
				        containerResize: 1,
				        width: _width,
				        after: function(curr, next, obj) {
				            window.activeSlide = obj.currSlide;
				        },
				        startingSlide: window.activeSlide
				    });

				},500);
			}		   
		}
	}
	else{
		
		if($('body').hasClass('home')){
			$('#images').cycle({
				fit : 1,
			    containerResize: 1,
			    after: function(curr, next, obj) {
			        window.activeSlide = obj.currSlide;
			    }
			});
		
			$("#nav-toggle").on('click',function(){
				home_page($,true);
				home_page($,true);
			});

			$("#social .tw").on('click',function(){
	    		popupwindow('http://www.twitter.com/share?url=http://bit.ly/15ac9mt&text=Dream On create great mattresses, mattress toppers and pillows. Check out their website for more info ','twittershare',728,392);
	    		return false;
	    	});

	    	$('#share_button').click(function(e){
				e.preventDefault();
				FB.ui({
					method: 'feed',
					name: 'Dream On',
					link: 'http://dream-on.sbdigi.com',
					picture: 'http://dream-on.sbdigi.com/wp-content/themes/dreamon/images/logo.png',
					description: 'We are Dream On. We create great mattresses, mattress toppers and pillows. With over 12 years experience, we have built an extensive knowledge of what the body needs for a good night’s sleep. It is with this knowledge that we have built a range of products to suit everyone’s needs.',
					message: ''
				});
			});
		}

        $("#images img").load (function(){
            var height = $("#images img").height();
            $("#images").height(height);
        }).each(function() {
            if(this.complete) {
                $(this).load();
            }
        });
	}
}
function popupwindow(url, title, w, h) {
	var left = (screen.width/2)-(w/2);
	var top = (screen.height/2)-(h/2);
	return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
}

//================================================== OUR RANGE PAGE
function our_range_page($,resize){
	if(resize){
		_width = $(".page-template-page-products-php #products-thumb .entry.active").width()-12;
    	_height = $(".page-template-page-products-php #products-thumb .entry.active").height()-12;
    	$(".page-template-page-products-php #products-thumb .entry.active a").css({
    		width : _width,
    		height : _height
    	});
    	$("#products-details .image").css({
    		width : ((_width+12)*2)+16
    	})
    	$("#products-details .details").css({
    		width : _width+12
    	});
    	/*    
    	console.log('asdf');
    	if($("#main").width()<=mobile_size){
    		$("#products-thumb .entry.active").each(function(){
    			$(this).parent().hide();
    		});
    	}*/
    	// EVERY SIZE SHOULD RESET THE STATE OF OUR RANGE PAGE
    	if($("#main").width()<=mobile_size){
    		$("#products-thumb").fadeIn();
    	}
//    	our_page_reset($);
        $('.basic-info.scroll').jScrollPane();
        $('.more-info .scroll').jScrollPane();
	}
	else{

		if($('body').hasClass('page-template-page-products-php')){
			$("#nav-toggle").on('click',function(){
				// FIX FOR dimention of ACTIVE THUMB
				if($("#main").width()>mobile_size){
					setTimeout(function(){
						$("#products-thumb .entry.active a").each(function(){
							_width = $(this).parent().width()-12;
					    	_height = $(this).parent().height()-12;

					    	$(this).css({
					    		width : _width,
					    		height : _height,
					    	});
						})
					},200);					
				}
				// EVERY CLICK ON NAV SHOULD RESET THE STATE OF THE RANGE PAGE EXCEPT FOR THE NAV
				our_page_reset($);
			});
		
		
			$("#products-thumb .entry a").on('click',function(){

				// IN EVERY CLICK, more info should be closed
				$(".more-info").removeAttr('style');

				// DISPLAYING ENTRIES but parent's are still hiden
				pid = $(this).data('post-id');
				$('#mobile-details .entry, #products-details .entry').hide().filter(".p"+pid).fadeIn();

				if($("#main").width()<=mobile_size){

					// SHOWING DETAIL MOBILE
					$("#mobile-details").css("opacity","1").fadeIn();
		    		$("#products-thumb, #products-details").hide();
				}
				else{

					// SHOWING DETAIL DESKTOP
					$("#products-details, #products-thumb").fadeIn();
		    		$("#mobile-details").hide();

					// HIGHLIGHTS the thumb images
					$('#products-thumb .entry').removeClass('active');
					$(this).parent().addClass('active');

					_width = $(this).parent().width()-12;
			    	_height = $(this).parent().height()-12;

			    	$(this).css({
			    		width : _width,
			    		height : _height
			    	});
			    	$("#products-details .image").css({
			    		width : ((_width+12)*2)+16
			    	})
			    	$("#products-details .details").css({
			    		width : _width+12
			    	})
				}
				// ENABLE SCROLLPANE WHEN DETAILS ARE ALREADY SHOWING. the block below is for jscrollpane hack
//
//				$(".page-template-page-products-php #products-details .entry").each(function(){
//					_img_height  = $(this).find('.image').height();
//				});
				//if($("#main").width()>ipad_size){
					$('.basic-info.scroll').jScrollPane();					
				//}
				$('.more-info .scroll').jScrollPane();
				// end jscrollpan

				// ANIMATION FOR CLOSE BUTTON
				$(".close-detail")
					.css("transition-delay", "1s")
					.css("-webkit-transition-delay", "1s") /* Safari */
					.css({right:0, opacity:1});		
				$('#products-thumb .entry').removeClass('active');
				$(this).parent().addClass('active');

				// LOOK AT THE TOP
				$('html,body').animate({
		            scrollTop: 0
		        }, 750);

				return false;
			});
		}

		// DESKTOP AND MOBILE DETAILS
		$(".close-detail").on('click',function(){
			$(this)
				.css("transition-delay", "0s")
				.css("-webkit-transition-delay", "0s") /* Safari */
				.css({right:"-581px", opacity:0});
			
			if($("#main").width()<=mobile_size){
				$('#products-thumb').fadeIn();	
			}
			$(this).parent().hide();
			$('#products-thumb').hide().fadeIn().find('.entry').removeClass('active');	

			// EVERY HIT CLOSE SHOULD RESET THE STATE OF OUR RANGE PAGE
			our_page_reset($);	
//			our_page_entry_sizes($);
			return false;
		});

		// DESKTOP DETAIL		
		$("a.more-info").on('click',function(){
			$(this).parent().parent().parent().parent().parent().find('div.more-info').css({
				top: "160px",
				opacity:1
			});

            setTimeout(function(){
                $('.more-info .scroll').jScrollPane();
            },250);

			return false;
		});
		$("a.less-info").on('click',function(){
			$(this).parent().parent().css({
				top:"1300px"
			});
			return false;
		});

		// DETAIL NUMBERED ITEMS
		$("ol").each(function(){
			$(this).find('li').each(function(_eq){
				$(this).prepend("<span class='eq'>"+(_eq+1)+"</span> ");	
			});		
		});

	}
}
function our_page_reset($){
	$("#mobile-details, #products-details").hide();
	$(".entry").removeClass('active').removeAttr('style');
	$("#products-thumb a").removeAttr('style');
	$(".details").removeAttr('style');
}



//================================================== ABOUT PAGE
function about_page($){
	$("a[data-type='popover']").popover({
		trigger : 'hover',
		html : true,
		placement : 'top',
		delay	: { show: 0, hide: 800 }
	});
}


//================================================== MENU
function menu($,resize){

	if(!resize){
		current_page = $(".current-menu-item a").offset();	
		$("#left-notch").css({
			top:current_page.top+10,
			transition:"all 0.2s linear"
		});
	}

	if($(window).width()<ipad_size){
		$("#header").removeClass('show');
		$("#main").addClass('full');
	}
	else{
		$("#header").addClass('show');
		$("#main").removeClass('full');
	}

	$("#access a").on('click',function(){
		$("#access a").removeClass('active');
		$("#access li").removeClass('current-menu-item');
		$(this).addClass('active');

		$("#left-notch").css({			
			top:$(this).offset().top+10
		});
	});

	$("#nav-toggle").on('click',function(){
		if($("#header").hasClass('show')){
			$("#header").removeClass('show');
			$("#main").addClass('full');
		}
		else{
			$("#header").addClass('show');
			$("#main").removeClass('full');		
		}
		//responsive_home_main($); // HACK, need to call this function twice for some reason to fix the issue on slider responsive.
	  	//responsive_home_main($); // here's the second call.
		
		return false;
	});
}


//================================================== CONTACT PAGE
function initialize() {
		var styles = [
		{
			featureType: "road",
			elementType: "geometry.stroke",
			stylers: [
			  	{ color: '#d7cfc7'}
			]
		},
		{
		    featureType: 'landscape',
		    elementType: 'geometry',
		    stylers: [
		      { color: '#ffffff' }
		    ]
		},
		{
		    featureType: 'water',
		    elementType: 'geometry',
		    stylers: [
		      { color: '#d7d6ff' }
		    ]
		}];
		var mapOptions = {
		  center: new google.maps.LatLng(lat,-0.289412),
		  zoom: 17,
		  disableDefaultUI: true,
		  zoomControl: true,
		    zoomControlOptions: {
		        style: google.maps.ZoomControlStyle.LARGE,
		        position: google.maps.ControlPosition.BOTTOM_RIGHT
		    },
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		};

		map = new google.maps.Map(document.getElementById("map-canvas"),
		    mapOptions);

		map.setOptions({styles: styles});

		
		// CUSTOM MARKER
		//var iconBase = 'http://localhost/dreamon/wp-content/themes/dreamon/images/';
		var iconBase = 'http://dream-on.sbdigi.com/wp-content/themes/dreamon/images/';

		if(jQuery("#main").width()<=mobile_size){
			var mapmarker = "mapmarker-mobile";
		}
		else{
			var mapmarker = "mapmarker";
		}

		var marker = new google.maps.Marker({
		  position: new google.maps.LatLng(lat,lon),
		  map: map,
		  icon: iconBase + mapmarker+'.png',
		  shadow: iconBase + 'mapmarker-shadow.png'
		});	
}

function contact_page($, resize){
	
	if(resize){
		if($("#main").width()<=ipad_size){
    		$(".one_half, .one_half.last").css({
    			width: '100%',
				margin: 0,
				padding: 0,
				border: "none"
    		});
    	}
    	else{
    		$(".one_half, .one_half.last").removeAttr('style');
    	}

    	// GMAP FIX
    	if($("#gmap").html()!=undefined){
    		google.maps.event.trigger(map, 'resize');
			map.setCenter(new google.maps.LatLng(lat,lon));	
    	}
	}
	else{
		if($("#main").width()<=ipad_size){
    		$(".one_half, .one_half.last").css({
    			width: '100%',
				margin: 0,
				padding: 0,
				border: "none"
    		});
    	}
    	$("#nav-toggle").on('click',function(){
    		google.maps.event.trigger(map, 'resize');
			map.setCenter(new google.maps.LatLng(lat,lon));
    	});

    	$("#map-locator").on('click',function(){
			google.maps.event.trigger(map, 'resize');
			map.setCenter(new google.maps.LatLng(lat,lon));
		})
	}

}