<?php

function front_styles(){
    wp_enqueue_script('jquery');
	wp_enqueue_script("superfish",BASE_URL."/js/superfish.js");
    wp_enqueue_script("supersubs",BASE_URL."/js/supersubs.js");
    wp_enqueue_script("hoverIntent",BASE_URL."/js/hoverIntent.js");
    wp_enqueue_script("cycle",BASE_URL."/js/jquery.cycle.all.js");
    wp_enqueue_script("base",BASE_URL."/js/base.js");
    wp_enqueue_script("bootstrap",BASE_URL."/js/bootstrap.js");
    wp_enqueue_script("modernizr",BASE_URL."/js/modernizr.min.js");
    wp_enqueue_script("main",THEME_URL."/js/main.js");
    // Responsive
    wp_enqueue_script("fitimage",BASE_URL."/js/jquery.imagefit.js");
    //wp_enqueue_script("jquerynivoslider",BASE_URL."/slider/jquery.nivo.slider.js");

    wp_enqueue_style( 'shortcodes', BASE_URL.'/css/shortcodes.css');
    wp_enqueue_style("superfish",BASE_URL."/css/superfish.css");
    wp_enqueue_style("animate",BASE_URL."/css/animate.css");
    wp_enqueue_style("base",BASE_URL."/css/base.css");    
    wp_enqueue_style("style",THEME_URL."/style.css");  
} add_action('wp_enqueue_scripts', 'front_styles');

add_filter('body_class','browser_body_class');
function browser_body_class($classes) {
    global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
    if($is_lynx) $classes[] = 'lynx';
    elseif($is_gecko) $classes[] = 'gecko';
    elseif($is_opera) $classes[] = 'opera';
    elseif($is_NS4) $classes[] = 'ns4';
    elseif($is_safari) $classes[] = 'safari';
    elseif($is_chrome) $classes[] = 'chrome';
    elseif($is_IE) {
        $classes[] = (ereg('MSIE 9',$_SERVER['HTTP_USER_AGENT'])) ? 'ie9' : 'ie';
        $classes[] = (ereg('MSIE 8',$_SERVER['HTTP_USER_AGENT'])) ? 'ie8' : 'ie';
    }
    else $classes[] = 'unknown';
    if($is_iphone) $classes[] = 'iphone';

    return $classes;
}

/* atom_title() - This is responsible for <title> text display for SEO purpose. 
                - This can be changed in Theme Panel > SEO
                - Can be: 
                    1. Home Title
                        a) Blog Name | Blog Description 
                        b) Blog description | BlogName
                        c) BlogName only
                    2. Single Title
                        a) Blog Name | Post Title 
                        b) Post title | BlogName
                        c) Post title only
                    3. Index Title (categories,archives,search results)
                        a) BlogName | Category name
                        b) Category name | BlogName
                        c) Category name only */                    
function atom_title(){
    $temp = '';
    $sep = get_atom_option('homepage_title_separator','|');
    $site_description = get_bloginfo('description');
    $blog_name=get_bloginfo( 'name' );
    if(is_home() || is_front_page()){       
        if(get_atom_option('homepage_set_custom_title')=="on"){
            $temp=get_atom_option("homepage_custom_title");
        }
        else {
            $temp_title = get_atom_option('homepage_title_type','blogname_|_blog_description');     
            if($temp_title=='blogname_|_blog_description')
                $temp=$blog_name.' '.$sep.' '.$site_description;
            elseif($temp_title=='blog_description_|_blogname')
                $temp=$site_description.' '.$sep.' '.$blog_name;
            else
                $temp=$blog_name;
        }
    }
    elseif(is_single() || is_page()){       
        $temp_title = get_atom_option('single_title',1);        
        if($temp_title==0){
            $temp = $blog_name . wp_title( $sep, false, 'left' );
        }
        elseif($temp_title==1){
            $temp = wp_title( $sep, false, 'right' ) . $blog_name ;
        }
        else{
            $temp=$blog_name;
        }       
    }
    else{
        $temp_title = get_atom_option('index_title',1);     
        if($temp_title==0){
            $temp = $blog_name . wp_title( $sep, false, 'left' );
        }
        elseif($temp_title==1){
            $temp = wp_title( $sep, false, 'right' ) . $blog_name ;
        }
        else{
            # Spacing issue here is solved on the trim() func on the nearest code.
            $temp=wp_title( '',false,'' );
        }   
    }
    echo trim($temp); 
}
function atom_description(){
    if(is_home() || is_front_page()){
        $temp = get_atom_option('homepage_set_meta_description');
        if($temp=="on") {
            ?><meta name="description" content="<?php echo get_atom_option('homepage_meta_description') ?>" /><?php echo "\n";
        }
    }
    elseif(is_single() || is_page()){       
    }
    else{
    }
}
function atom_keywords(){
    if(is_home() || is_front_page()){
        $temp = get_atom_option('homepage_set_meta_keywords');
        if($temp=="on") {
            ?><meta name="keywords" content="<?php echo get_atom_option('homepage_meta_keywords') ?>" /><?php echo "\n";
        }
    }
    elseif(is_single() || is_page()){       
    }
    else{
    }
}
function atom_canonical(){
    if(is_home() || is_front_page()){
        $temp = get_atom_option('homepage_set_canonical');
        if($temp=="on") {
            ?><link rel="canonical" href="<?php echo get_bloginfo('siteurl') ?>" /><?php echo "\n";
        }
    }
    elseif(is_single() || is_page()){       
    }
    else{
    }
}
function get_atom_option($option,$default=''){
    $arr=get_option(strtolower(THEME_NAME)."_settings");
    if(!isset($arr[$option]) || $arr[$option]==''){
        return $default;
    }
    else{
        return atom_string($arr[$option]);      
    }
}