(function() {
    tinymce.create('tinymce.plugins.shortcodes', {  
        init : function(ed, url) {  
            ed.addButton('blog', {  
                title : 'Add a Blog',  
                image : url+'/../lib/shortcodes/images/blogger.png',  
                onclick : function() { 
					loadOptions('blog');
                }  
            });  
			ed.addButton('box', {  
                title : 'Add a Box',  
                image : url+'/../lib/shortcodes/images/box.png',  
                onclick : function() {  
                     loadOptions('box');
                }  
            });  
			ed.addButton('button', {  
                title : 'Add a Button',  
                image : url+'/../lib/shortcodes/images/btn.png',  
                onclick : function() {  
                     loadOptions('button');
                }  
            });  
			ed.addButton('gchart', {  
                title : 'Add Google Chart',  
                image : url+'/../lib/shortcodes/images/chart.png',  
                onclick : function() {  
                     loadOptions('gchart');
                }  
            }); 
			ed.addButton('gmap', {  
                title : 'Add Google Map',  
                image : url+'/../lib/shortcodes/images/map.png',  
                onclick : function() {  
                     loadOptions('gmap');
                }  
            });  
			ed.addButton('image', {  
                title : 'Add Image Shortcode',  
                image : url+'/../lib/shortcodes/images/image.png',  
                onclick : function() {  
                     loadOptions('image');
                }  
            });  
			ed.addButton('sidebar', {  
                title : 'Add Sidebar',  
                image : url+'/../lib/shortcodes/images/sidebar.png',  
                onclick : function() {  
                     loadOptions('sidebar');
                }  
            });  
			ed.addButton('slider', {  
                title : 'Add Slider',  
                image : url+'/../lib/shortcodes/images/slider.png',  
                onclick : function() {  
                     loadOptions('slider');
                }  
            });  
			ed.addButton('video', {  
                title : 'Add Video Shortcode',  
                image : url+'/../lib/shortcodes/images/video.png',  
                onclick : function() {  
                     loadOptions('video');
                }  
            });  
        },  
        createControl : function(n, cm) {  
            return null;  
        },  
    });  
    tinymce.PluginManager.add('shortcodes', tinymce.plugins.shortcodes);  
})(); 
function ucwords (str) {
    return (str + '').replace(/^([a-z])|\s+([a-z])/g, function ($1) {
        return $1.toUpperCase();
    });
}