<!DOCTYPE HTML>
<html lang="en">
<head>
	<title>Dream On</title>
	<?php atom_description(); ?>
	<?php atom_keywords(); ?>
	<?php atom_canonical(); ?>
	<meta charset="utf-8">
	<meta name="author" content="Themetribe">
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/base/css/bootstrap.css" />
	<link type="text/css" href="<?php echo THEME_URL ?>/css/jquery.jscrollpane.css" rel="stylesheet" media="all" />
	<script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsmk4JFK1keSiT-gtAz5_c3_ABGAGAsUg&sensor=false">
    </script>
	<?php wp_head(); ?>
	<script type="text/javascript" src="<?php echo THEME_URL ?>/js/jquery.mousewheel.js"></script>
	<script type="text/javascript" src="<?php echo THEME_URL ?>/js/jquery.jscrollpane.min.js"></script>
	<script type="text/javascript" src="//use.typekit.net/pkz1mut.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
   
   	<meta property="og:title" content="We are Dream On"/>
   	<meta property="og:site_name" content="Dream On"/>
   	<meta property="og:url" content="http://dream-on.sbdigi.com"/>
   	<meta property="og:type" content="blog"/>
   	<meta property="og:image" content="http://dream-on.sbdigi.com/wp-content/themes/dreamon/images/logo.png"/>
   	<meta property="og:description" content="We are Dream On. We create great mattresses, mattress toppers and pillows. With over 12 years experience, we have built an extensive knowledge of what the body needs for a good night’s sleep. It is with this knowledge that we have built a range of products to suit everyone’s needs." />
    
</head>
<body <?php body_class(); ?>>	
	<header id="header" class="clearfix show">
		<a id="nav-toggle" href="#"><!-- empty --></a>
		<?php site_title_logo(); ?>
		<nav id="access">
			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'sf-menu' ) ); ?>
			
		</nav>

		<div id="left-notch">
			<span class="top"></span>
			<span class="notch"></span>
			<span class="bottom"></span>
		</div>
	</header>